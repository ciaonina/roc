﻿/// <reference path="../Scripts/typings/requirejs/require.d.ts" />
/// <reference path="./App.ts" />
require(
    [
		"App"
    ],
    (App) => {
        "use strict";
        const app = new App();
    }
);