﻿"use strict";

import Helper = require("Helper");
import DayNightServiceProviderOption = require("DayNight/DayNightServiceProviderOption");

class DayNightServiceProvider {
	private game: Phaser.Game;
	private dayLenght: number;
	private shading: any[];
	private sunTween: Phaser.Tween;
	private moonTween: Phaser.Tween;
	private hour = 0;
	private minute = 0;
	private timeLapse: number;
	private clockTime: Phaser.Text;
	private timerEvent: Phaser.TimerEvent;
	private hourEvent: Phaser.TimerEvent;
	private clockText = "";

    currentDate = new Date();
    motionSpeed: number;
    sunRiseSignal = new Phaser.Signal();
    moonRiseSignal = new Phaser.Signal();
	minuteSignal = new Phaser.Signal();
	clockSignal = new Phaser.Signal();
    sunSprite: Phaser.Sprite;
    moonSprite: Phaser.Sprite;

	constructor(game: Phaser.Game,
		dayLenght: number,
		options: DayNightServiceProviderOption
	) {
		this.game = game;
		this.dayLenght = dayLenght;
		this.shading = null;
		this.sunSprite = null;
		this.moonSprite = null;
        this.timeLapse = this.dayLenght / 12;
		this.motionSpeed = dayLenght + (dayLenght / 32);
		this.initClockText();
        this.initShading(options.backgroundSprites);
        this.initSun(options.sunSprite);
        this.initMoon(options.moonSprite);
	}

	initClockText() {
        //this.clockText = this.currentDate.toDateString() + " " + (this.hour < 10 ? `0${this.hour}` : this.hour + "") + ":00";
        //this.clockTime = this.game.add.text(10, 10, this.clockText, null);
        //this.clockTime.fontSize = 12;
        //this.clockTime.addColor("white", 0);
	}

	initSun(sprite: Phaser.Sprite) {
		this.sunSprite = sprite;
		this.sunRise(sprite);
	}

	initMoon(moon: Phaser.Sprite) {
		this.moonSprite = moon;
		this.moonSprite.tint = Helper.RGBtoHEX(243, 234, 135);
		this.moonSet(moon);
	}

	sunRise(sprite: Phaser.Sprite) {
        this.currentDate.setDate(this.currentDate.getDate() + 1);
        this.sunRiseSignal.dispatch(this.currentDate);
		sprite.position.x = 20 + (sprite.width / 2);
		sprite.anchor.setTo(0.5, 0.5);
		this.sunTween = this.game.add.tween(sprite).to({ y: 0 - (sprite.height * 2) }, this.motionSpeed, Phaser.Easing.Circular.Out, true);
		this.sunTween.onComplete.add(this.sunSet, this);
        if (this.shading) {
            this.shading.forEach((sprite: any) => {
                Helper.tweenTint(this.game, sprite.sprite, sprite.from, sprite.to, this.motionSpeed, Phaser.Easing.Linear.None);
            });
        }
        if (this.timerEvent == null) {
            this.timerEvent = this.game.time.events.loop(this.timeLapse, () => {
				if (this.hour === 24) {
                    this.hour = 0;
				} else {
                    this.hour++;
                    if (this.hourEvent == null) {
                        this.hourEvent = this.game.time.events.loop(this.timeLapse / 60, () => {
                            if (this.minute === 60) {
                                this.minute = 0;
							} else {
                                this.minute++;
                                this.minuteSignal.dispatch({ minute: this.minute, hour: this.hour });
							}
						});
					}
                    this.clockSignal.dispatch(this.hour);
				}
                //this.clockText = (this.hour < 10 ? `0${this.hour}` : this.hour + "") + ":00";
                //this.clockTime.text = this.currentDate.toDateString() + " " + this.clockText;
			});
		}
	}

	sunSet(sprite: Phaser.Sprite) {
		sprite.position.x = this.game.width - (sprite.width / 2)-10;
		this.sunTween = this.game.add.tween(sprite)
            .to({ y: this.game.world.height + (sprite.height * 2) }, this.motionSpeed, Phaser.Easing.Linear.None, true);

		this.sunTween.onComplete.add(this.sunRise, this);
		if (this.shading) {
			this.shading.forEach((sprite: any) => {
                Helper.tweenTint(this.game, sprite.sprite, sprite.to, sprite.from, this.motionSpeed, Phaser.Easing.Linear.None);
			});
		}
	}

	moonRise(sprite: Phaser.Sprite) {
        sprite.position.x = 20;
        this.moonRiseSignal.dispatch(this.currentDate);
		this.moonTween = this.game.add.tween(sprite).to({ y: 0 - (sprite.height * 2) }, this.motionSpeed, Phaser.Easing.Linear.None, true);
		this.moonTween.onComplete.add(this.moonSet, this);

	}

	moonSet(sprite: Phaser.Sprite) {
		sprite.position.x = this.game.width - (sprite.width + 20);
		this.moonTween = this.game.add.tween(sprite)
			.to({ y: this.game.world.height + (sprite.height * 2) }, this.motionSpeed, Phaser.Easing.Linear.None, true);
		this.moonTween.onComplete.add(this.moonRise, this);
	}

	initShading(sprites: any[]) {
		this.shading = sprites;
	}
}

export = DayNightServiceProvider;