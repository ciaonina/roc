﻿"use strict";

 class DayNightServiceProviderOption {
    backgroundSprites: any[];
    sunSprite: Phaser.Sprite;
    moonSprite: Phaser.Sprite;
    starsSprite: Phaser.Sprite;

    constructor(backgroundSprites: any[],
        sunSprite: Phaser.Sprite,
        moonSprite: Phaser.Sprite,
        starsSprite: Phaser.Sprite
    ) {
        this.backgroundSprites = backgroundSprites;
        this.sunSprite = sunSprite;
        this.moonSprite = moonSprite;
        this.starsSprite = starsSprite;
    }
}

 export = DayNightServiceProviderOption;