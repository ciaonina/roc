﻿import DTO = require("Company/DTO/Company");
import CompanyBuilder = require("Company/CompanyBuilder");
import Constants = require("Common/Constant");
import FactServiceProvider = require("Facts/FactServiceProvider");
import Helper = require("Helper");

module CompanyServiceProvider {
    "use strict";
    export class CompanyServiceProvider {
        gameOverSignal = new Phaser.Signal();
        moneyChangeSignal = new Phaser.Signal();
        growSignal0 = new Phaser.Signal();
        growSignal1 = new Phaser.Signal();
        growSignal2 = new Phaser.Signal();

        shitHappened = new Phaser.Signal();
        company: DTO.Company;
        companyBuilder = new CompanyBuilder.CompanyBuilder();
        factServiceProvider = new FactServiceProvider.FactServiceProvider();

        createCompany() {
            this.company = this.companyBuilder.createCompany();
        }

        update(hour: number, factEnabled: boolean) {
            this.company.evaluate(hour, factEnabled);
            this.moneyChangeSignal.dispatch(this.company.$, this.company.done);


            if (this.company.$ < Constants.Constants.$STATE_1) {
                this.growSignal0.dispatch();
            }

            if (this.company.$ > Constants.Constants.$STATE_1 && this.company.$ < Constants.Constants.$STATE_2) {
                this.growSignal1.dispatch();
            }

            if (this.company.$ > Constants.Constants.$STATE_2) {
                this.growSignal2.dispatch();
            }

            if (this.company.$ >= Constants.Constants.$GAMEOVER) {
                this.gameOverSignal.dispatch();
            }
        }

        evaluateNewFact(hour: number) {
            if (!this.factServiceProvider.evaluating && (hour > 9 && hour < 17)) {
                if (Helper.RandomMinMax(1, 100) <= 33) {
                    this.shitHappened.dispatch(this.factServiceProvider.createNewFact());
                }
            }
        }
    }

}
export = CompanyServiceProvider;