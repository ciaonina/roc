﻿
import Helper = require("Helper");
import Constants = require("Common/Constant");
module Company {
    "use strict";

    export enum eGrow {
        Stage1 = 1,
        Stage2 = 2,
    }

    export class Company {
        name: string;
        $: number = 0;
        done: number = 0;
        evaluate(hour: number, factEnabled: boolean) {
            let value = 1;
            if (factEnabled) {
                value = Helper.RandomMinMax(Constants.Constants.MONEY_MIN, Constants.Constants.MONEY_MAX);
            }
            this.$ += value;
        }
    }
}

export = Company;