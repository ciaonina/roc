﻿
import DTO = require("Company/DTO/Company");
import Constants = require("Common/Constant");
module Company {
    "use strict";
    export class CompanyBuilder {
        createCompany(): DTO.Company {
            const result = new DTO.Company();
            result.$ = Constants.Constants.MONEY_START;
            result.name = "Evil Corporation";
            return result;
        }
    }
}

export = Company;