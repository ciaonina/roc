﻿/// <reference path="../Scripts/typings/phaserjs/phaser.d.ts" />

"use strict";

import Boot = require("Boot");
import Preloader = require("Preloader");
import Stage1 = require("Stages/Stage1");

class App extends Phaser.Game {
	constructor() {
		super("100%", "100%", Phaser.AUTO, "content", null);
		this.state.add("Boot", Boot, false);
		this.state.add("Preloader", Preloader, false);
		this.state.add("Stage1", Stage1, false);
        this.state.start("Boot", true, false);

    }

    create() {
        this.stage.backgroundColor = "#ff0000";
    }
}
export = App;
