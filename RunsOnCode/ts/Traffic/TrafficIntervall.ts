﻿
namespace Traffic {
    "use strict";
    export class TrafficIntervall {
        limit: number;
        condition: () => boolean;

        constructor(condition: () => boolean, limit: number) {
            this.condition = condition;
            this.limit = limit;
        }
    }
}
export = Traffic;