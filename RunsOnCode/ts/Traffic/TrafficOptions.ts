﻿import TfTrafficIntervall = require("Traffic/TrafficIntervall");

namespace Traffic {
    "use strict";
    export class TrafficOptions {
        date: Date;
        hour: number;
        putOnTop: boolean;
        intervals: TfTrafficIntervall.TrafficIntervall[];
        constructor(date: Date, hour: number, putOnTop: boolean) {
            this.date = date;
            this.hour = hour;
            this.putOnTop = putOnTop;
            this.intervals = [];
        }
    }
}
export = Traffic;