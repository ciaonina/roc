﻿namespace Traffic {
    "use strict";

    export class CarOptions {
        prevX: number;
        y: number;
        anchorX: number;
        nextX: number;
        nextW: number;
        nextH: number;
        scaleX: number;
        putOnTop: boolean;
        speed: number;
        easing: any;
        selector: string;

        constructor(prevX: number, y: number, anchorX: number, nextX: number, nextW: number, nextH: number, scaleX: number,
            putOnTop: boolean,
            speed: number,
            easing: any,
            selector: string
        ) {
            this.prevX = prevX;
            this.y = y;
            this.anchorX = anchorX;
            this.nextX = nextX;
            this.nextW = nextW;
            this.nextH = nextH;
            this.scaleX = scaleX;
            this.putOnTop = putOnTop;
            this.speed = speed;
            this.easing = easing;
            this.selector = selector;
        }
    }
}

export = Traffic;