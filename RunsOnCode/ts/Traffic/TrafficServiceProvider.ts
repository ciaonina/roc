﻿"use strict";

import Helper = require("Helper");
import TrafficOptions = require("Traffic/TrafficOptions");
import TfCarOptions = require("Traffic/CarOptions");
import TfCarBuilder = require("Traffic/CarBuilder");

class TrafficServiceProvider {
    private game: Phaser.Game;
    private dayLight: number;
    private carSprites: Phaser.Group;
    private trafficClip: Phaser.Sound = null;

    constructor(game: Phaser.Game, dayLight: number) {
        this.game = game;
        this.dayLight = dayLight;
        this.carSprites = this.game.add.group();

    }

    update(options: TrafficOptions.TrafficOptions) {
        const chance = Helper.RandomMinMax(1, 1000) < 100;
        if (this.trafficClip == null) {
            this.trafficClip = this.game.add.audio("bangalore", 0.1, true);
            this.trafficClip.play();
        }

        if (chance) {
            const isRightToLeft = Helper.RandomMinMax(1, 2) === 2;
            const selector = `car-${Helper.RandomMinMax(1, 4)}`;
            const car = this.game.cache.getImage(selector);
            const nextH = options.putOnTop ? 20 : 10;
            const nextW = (car.width * nextH) / car.height;
            let prevX = 0;
            let nextX = this.game.width;
            const speed = this.dayLight / 24;
            const easing = Phaser.Easing.Default;
            let anchorX = 0;
            let scaleX = 1;

            if (isRightToLeft) {
                prevX = this.game.width + nextW;
                nextX = 0 - nextW;
                anchorX = 1;
            } else {
                prevX -= nextW;
                nextX += nextW;
                scaleX = -1;
            }
            const carOptions = new TfCarOptions.CarOptions(
                prevX,
                this.game.height - nextH,
                anchorX,
                nextX,
                nextW,
                nextH,
                scaleX,
                options.putOnTop,
                speed,
                easing,
                selector
            );
            const carSprite = new TfCarBuilder.CarBuilder(this.game, this.dayLight, carOptions).create();
            if (options.putOnTop) {
                this.carSprites.add(carSprite);
            }
            this.game.add.tween(carSprite).to({ x: nextX }, (speed * Helper.RandomMinMax(1, 100)) / 10, easing, true)
                .onComplete.add((s: Phaser.Sprite, t: any) => {
                    s.destroy();
                });
        }
    }

    putOnTop() {
        this.game.world.bringToTop(this.carSprites);

    }
}

export = TrafficServiceProvider;