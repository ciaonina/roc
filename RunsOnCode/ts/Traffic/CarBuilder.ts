﻿import TfCarOptions = require("Traffic/CarOptions");
import Helper = require("Helper");

namespace Traffic {
    "use strict";
    export class CarBuilder {
        game: Phaser.Game;
        dayLenght: number;
        options: TfCarOptions.CarOptions;
        helper: Helper;
        constructor(game: Phaser.Game, dayLenght: number, options: TfCarOptions.CarOptions) {
            this.game = game;
            this.dayLenght = dayLenght;
            this.options = options;
        }

        create(): Phaser.Sprite {
            const result = this.game.add.sprite(this.options.prevX, this.game.height - this.options.nextH, this.options.selector);
            result.anchor.x = this.options.anchorX;
            result.x = this.options.prevX;
            result.width = this.options.nextW;
            result.height = this.options.nextH;
            result.scale.x *= this.options.scaleX;
            let limit = 256;
            if (!this.options.putOnTop) {
                 limit = 0;
            }
            const r = Helper.RandomMinMax(0, limit);
            const g = Helper.RandomMinMax(0, limit);
            const b = Helper.RandomMinMax(0, limit);
            result.tint = Helper.RGBtoHEX(r, g, b);
            return result;
        }

    }

}


export = Traffic;