﻿import Helper = require("Helper");
import Enums = require("Meteo/Enums/enums");
import DTO = require("DTO/dtos");

module Meteo {
    "use strict";

    export class RainServiceProvider {
        private game: Phaser.Game;
        private emitter: Phaser.Particles.Arcade.Emitter = null;
        private bitmapData: Phaser.BitmapData = null;
        private dayLight: number;
        private fogSprite: Phaser.Sprite;
        private timer: Phaser.TimerEvent;
        private meteoContitions: DTO.MeteoResult = null;
        private rainClip: Phaser.Sound = null;
        private thunderClip: Phaser.Sound = null;
        constructor(game: Phaser.Game, dayLight: number) {
            this.game = game;
            this.dayLight = dayLight;
            this.timer = this.game.time.events.loop(0, () => {
                if (this.meteoContitions != null && this.meteoContitions.conditions === Enums.eMeteoConditions.Shitty) {
                    if (Helper.RandomMinMax(1, 1000) < 3) {
                        this.thunder();
                    }
                }
            }, this);
        }

        update(meteoContitions: DTO.MeteoResult) {
            if (meteoContitions.conditions === Enums.eMeteoConditions.Shitty) {
                this.meteoContitions = meteoContitions;
                this.start();
                this.thunder();
            }else {
                this.stop();
            }
        }

        putOnTop() {
            if (this.emitter != null) {
                this.game.world.bringToTop(this.emitter);
            }
            if (this.fogSprite) {
                this.game.world.bringToTop(this.fogSprite);
            }
        }

        start() {
            let rainLenght = 4000;
            if (this.emitter === null) {
                let emitter = this.game.add.emitter(0, 0, 1500);
                emitter.width = this.game.world.width * 2;
                emitter.x = this.game.world.centerX;
                this.emitter = emitter;
                this.bitmapData = this.game.add.bitmapData(this.game.world.width, rainLenght + 50);
            }

            if (this.emitter.on === false) {
                this.addTear(rainLenght, 0, 0);
                var constant = 300;
                for (let i = 0; i < this.emitter.width; i += constant) {
                    this.addTear(rainLenght, i, 0);
                }
                this.emitter.width = this.game.world.width;
                this.emitter.minParticleScale = 0.1;
                this.emitter.maxParticleScale = 0.1;

                let minYSpeed = 350;
                let maxYSpeed = 400;
                this.emitter.setYSpeed(minYSpeed, maxYSpeed);
                this.emitter.setAlpha(0.3, 0.5);

                var dir = Helper.RandomMinMax(1, 2);
                var minAngle: number, maxAngle: number;
                if (dir === 1) {
                    minAngle = -1;
                    maxAngle = -3;
                    this.emitter.pivot.x = 0;
                }else {
                    minAngle = 3;
                    maxAngle = 1;
                    this.emitter.pivot.x = 1;
                }
                this.emitter.angle = Helper.RandomMinMax(minAngle, maxAngle);
                this.emitter.minRotation = 0;
                this.emitter.maxRotation = 0;
                this.addFog();
            }

            this.emitter.makeParticles(this.bitmapData);
            this.game.time.events.add(this.dayLight / 2, () => {
                if (!this.emitter.on) {
                    this.emitter.start(false, this.dayLight, 1, 0);
                }
            }, this);
            if (this.rainClip == null) {
                this.rainClip = this.game.add.audio("rainroof",0.1, true);
            }
            this.rainClip.play();
        }

        stop() {
            if (this.emitter !== null && this.emitter.on) {
                this.emitter.on = false;
                this.removeFog();
            }
            if (this.rainClip != null) {
                this.rainClip.stop();
            }
            this.game.time.events.remove(this.timer);
        }

        private addFog() {
            const fog = this.game.add.bitmapData(this.game.width, this.game.height);
            fog.ctx.rect(0, 0, this.game.width, this.game.height);
            fog.ctx.fillStyle = "#666666"
            fog.ctx.fill();
            this.fogSprite = this.game.add.sprite(0, 0, fog);
            this.fogSprite.alpha = 0;
            this.game.add.tween(this.fogSprite).to({ alpha: 0.3 }, this.dayLight, null, true);
        }

        private removeFog() {
            const fogTween = this.game.add.tween(this.fogSprite).to({ alpha: 0 }, this.dayLight, null, true);
            fogTween.onComplete.add(() => {
                this.fogSprite.kill();
            }, this);
        }

        private thunder() {
            if (this.thunderClip == null) {
                this.thunderClip = this.game.add.audio("thunder",0.1);
            }
            this.thunderClip.play();
            this.game.camera.flash(0xffffff, Helper.RandomMinMax(3,5) * 100);
        }

        private addTear(rainLenght: number, x: number, y: number) {
            this.bitmapData.ctx.rect(x, y, 2, rainLenght);
            this.bitmapData.ctx.fillStyle = "#9cc9de";
            this.bitmapData.ctx.fill();
        }
    }
}

export = Meteo;