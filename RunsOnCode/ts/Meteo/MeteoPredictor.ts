﻿import Enums = require("Meteo/Enums/enums");
import DTO = require("Meteo/DTO/dtos");

module Meteo {
	"use strict";

	export class MeteoPredictor {
		private seasons: any[];
		private game: Phaser.Game;
		private dateDay: number = undefined;
		private duration: number = 0;
		private day: number = 0;
		private condition: Enums.eMeteoConditions;
		private spawnCloud: boolean;
		private cloudSpeed: number;
		private tint: boolean;

		constructor(game: Phaser.Game) {
			this.game = game;
			this.seasons = [];
			this.seasons.push(new DTO.SeasonInfo("Spring", 3));
			this.seasons.push(new DTO.SeasonInfo("Summer", 6));
			this.seasons.push(new DTO.SeasonInfo("Autumn", 9));
			this.seasons.push(new DTO.SeasonInfo("Winter", 12));
		}

		predict(date: Date, hour: number): DTO.MeteoResult {
			const result = new DTO.MeteoResult();
			let start = false;
            const currentDay = date.getDate();
			if (this.dateDay === undefined) {
				this.dateDay = currentDay;
				this.setCondition();
				result.shouldUpdate = true;
				start = true;
			}
			if (this.dateDay !== currentDay) {
				this.day++;
				this.dateDay = currentDay;
				if (this.duration === this.day) {
					this.day = 0;
					this.setCondition();
					result.shouldUpdate = true;
				}
			} else {
				if (!start) {
				    result.shouldUpdate = false;
				}
			}
			result.conditions = this.condition;
			this.setClouds();
			result.cloudSpeed = this.cloudSpeed;
			result.spawnCloud = this.spawnCloud;
			result.tint = this.tint;
			return result;
		}

		protected intInRange(min: number, max: number): number {
			return Math.floor(Math.random() * (max - min + 1)) + min;
		}

		setCondition() {
			this.duration = this.intInRange(1, this.intInRange(3, 10));
			const prob = this.intInRange(1, 100);
			if (prob <= 20) {
				this.condition = Enums.eMeteoConditions.Sunny;
			} else if (prob > 20 && prob <= 80) {
				this.condition = Enums.eMeteoConditions.ModeratelyCloudly;
			} else if (prob > 80 && prob <= 90) {
				this.condition = Enums.eMeteoConditions.Cloudly;
			} else if (prob > 90) {
				this.condition = Enums.eMeteoConditions.Shitty;
            }

            //this.condition = Enums.eMeteoConditions.Shitty;
		}

		setClouds() {
			this.tint = false;
			if (this.condition === Enums.eMeteoConditions.Sunny) {
				this.spawnCloud = this.intInRange(1, 10000) < 10;
			} else if (this.condition === Enums.eMeteoConditions.ModeratelyCloudly) {
				this.spawnCloud = this.intInRange(1, 10000) < 100;
			}else if (this.condition === Enums.eMeteoConditions.Cloudly) {
				this.spawnCloud = this.intInRange(1, 10000) < 500;
			}else if (this.condition === Enums.eMeteoConditions.Shitty) {
				this.spawnCloud = this.intInRange(1, 10000) < 8000;
				this.cloudSpeed = 10000;
				this.tint = true;
			}else if (this.condition === Enums.eMeteoConditions.Snow) {
				this.spawnCloud = this.intInRange(1, 100000) < 10;
			}
		}
	}
}
export = Meteo;