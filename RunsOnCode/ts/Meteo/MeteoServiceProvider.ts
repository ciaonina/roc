﻿import MPredictor = require("Meteo/MeteoPredictor");
import CloudServiceProvider = require("Meteo/CloudServiceProvider");
import RainServiceProvider = require("Meteo/RainServiceProvider");
import DTO = require("Meteo/DTO/dtos");
module Meteo {
    "use strict";

    export class MeteoServiceProvider {
        private game: Phaser.Game;
        private meteoPredictor: MPredictor.MeteoPredictor;
        private dayLight: number;
        cloudServiceProvider: CloudServiceProvider.CloudServiceProvider;
        rainServiceProvider: RainServiceProvider.RainServiceProvider;
        result: DTO.MeteoResult = null;

        constructor(game: Phaser.Game,
            cloudServiceProvider: CloudServiceProvider.CloudServiceProvider,
            rainServiceProvider: RainServiceProvider.RainServiceProvider,
            dayLight: number) {
            this.dayLight = dayLight;
            this.game = game;
            this.cloudServiceProvider = cloudServiceProvider;
            this.rainServiceProvider = rainServiceProvider;
            this.meteoPredictor = new MPredictor.MeteoPredictor(game);
        }
        update(date: Date, hour: number) {
            this.result = this.meteoPredictor.predict(date, hour);
            this.cloudServiceProvider.update(this.result);
            if (this.result.shouldUpdate) {
                this.rainServiceProvider.update(this.result);
            }
        }
        putOnTop() {
            this.rainServiceProvider.putOnTop();
        }
    }
}

export = Meteo;