﻿import Enums = require("Meteo/Enums/enums");

namespace Dto {
    "use strict";
    export class MeteoResult {
        conditions: Enums.eMeteoConditions;
        spawnCloud: boolean;
        shouldUpdate: boolean;
        tint: boolean;
        cloudSpeed = 0;
        constructor() {
            this.shouldUpdate = true;
        }
    }

    export class SeasonInfo {
        name: string;
        startMonth: number;
        constructor(name: string, startMonth: number) {
            this.name = name;
            this.startMonth = startMonth;
        }
    }
}

export = Dto;