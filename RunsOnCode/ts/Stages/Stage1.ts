﻿"use strict";

import DayNightServiceProvider = require("DayNight/DayNightServiceProvider");
import DayNightServiceProviderOption = require("DayNight/DayNightServiceProviderOption");
import TrafficServiceProvider = require("Traffic/TrafficServiceProvider");
import Helper = require("Helper");
import Constants = require("Common/Constant");
import MeteoServiceProvider = require("Meteo/MeteoServiceProvider");
import CloudServiceProvider = require("Meteo/CloudServiceProvider");
import RainServiceProvider = require("Meteo/RainServiceProvider");
import CompanyServiceProvider = require("Company/CompanyServiceProvider");
import FactServiceProvider = require("Facts/FactServiceProvider");
import BackgroundServiceProvider = require("Stages/BackgroundServiceProvider");
import EventServiceManager = require("Stages/EventServiceManager");
import TopBar = require("Stages/TopBar");
import DialogManager = require("Stages/DialogManager");

class Stage1 extends Phaser.State {
    dayNightServiceProvider: DayNightServiceProvider;
    trafficServiceProvider: TrafficServiceProvider;
    currentDate: Date = undefined;
    meteoServiceProvider: MeteoServiceProvider.MeteoServiceProvider;
    companyServiceProvider: CompanyServiceProvider.CompanyServiceProvider;
    factEnabled = false;
    backgroundServiceProvider: BackgroundServiceProvider;
    pushToTopSignal = new Phaser.Signal();
    dialogButtons: Phaser.Group = null;
    dontTrack: boolean;
    endSequenceHighLightSignal = new Phaser.Signal();
    allSequenceHighLightSignal = new Phaser.Signal();
    topBar : TopBar.TopBar;
    private eventServiceManager = new EventServiceManager.EventServiceManager(this);
    dialogManager: DialogManager.DialogManager;
    private music: Phaser.Sound = null;
    
    create() {
        if (this.music == null && Constants.Constants.MUSIC_ENABLED == true) {
            this.music = this.stage.game.add.audio("music_1", 0.1, true);
            this.music.play();
        }
        const dayLight = Constants.Constants.DAYLIGHT;
        this.backgroundServiceProvider = new BackgroundServiceProvider(this.game);
        this.dayNightServiceProvider = new DayNightServiceProvider(this.game,
            dayLight,
            new DayNightServiceProviderOption(
                this.backgroundServiceProvider.giveMeBackgroundSprites(),
                this.backgroundServiceProvider.sunSprite,
                this.backgroundServiceProvider.moonSprite, null
            )
        );
        const cloudServiceProvider = new CloudServiceProvider.CloudServiceProvider(this.game, dayLight);
        const rainServiceProvider =
            new RainServiceProvider.RainServiceProvider(this.game, dayLight);
        this.meteoServiceProvider =
            new MeteoServiceProvider.MeteoServiceProvider(this.game, cloudServiceProvider, rainServiceProvider, dayLight);
        this.trafficServiceProvider = new TrafficServiceProvider(this.game, dayLight);
        this.companyServiceProvider = new CompanyServiceProvider.CompanyServiceProvider();
        this.companyServiceProvider.createCompany();
        this.topBar = new TopBar.TopBar(this.game);
        this.eventServiceManager.registerEvents();
        this.dialogManager = new DialogManager.DialogManager(this);

    }

    update() {
        this.pushToTopSignal.dispatch();
    }
    
    execute(fact: FactServiceProvider.Fact) {
        this.companyServiceProvider.factServiceProvider.evaluating = true;
        this.dontTrack = true;
        this.dialogManager.drawButtons(fact);
    }
    
}

export = Stage1;