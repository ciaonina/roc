﻿"use strict";

import Constants = require("Common/Constant");

export class TopBar {
    score = 0;
    game: Phaser.Game;
    backgroundSprite: Phaser.Sprite;
    fillerSprite: Phaser.Sprite;
    moneyText: Phaser.BitmapText;
    sequenceText: Phaser.BitmapText;

    constructor(game: Phaser.Game) {
        this.game = game;
        this.drawBar();
        this.moneyText = this.game.add.bitmapText(10, 10, "digipip", "$:");
        this.moneyText.fontSize = Constants.Constants.FONTSIZE;
        this.moneyText.font = Constants.Constants.FONT;

        const seqText = "Seq:0";
        this.sequenceText = this.game.add.bitmapText(0, 10, "digipip", seqText);
        this.sequenceText.fontSize = Constants.Constants.FONTSIZE;
        this.sequenceText.font = Constants.Constants.FONT;
        this.sequenceText.x = this.game.width - (seqText.length * 20);
    }

    private drawBar() {
        const bgBitmap = this.game.add.bitmapData(this.game.width, Constants.Constants.BAR_HEIGHT);
        bgBitmap.ctx.rect(0, 0, this.game.width, Constants.Constants.BAR_HEIGHT);
        bgBitmap.ctx.fillStyle = Constants.Constants.BAR_BACKGROUND_COLOR;
        bgBitmap.ctx.fill();
        this.backgroundSprite = this.game.add.sprite(0, 0, bgBitmap);

        const fillerBitmap = this.game.add.bitmapData(this.game.width, Constants.Constants.BAR_FILLER_HEIGHT);
        fillerBitmap.ctx.rect(0, 0, this.game.width, Constants.Constants.BAR_FILLER_HEIGHT);
        fillerBitmap.ctx.fillStyle = Constants.Constants.BAR_FILLER_BACKGROUND_COLOR;
        fillerBitmap.ctx.fill();
        this.fillerSprite = this.game.add.sprite(0, Constants.Constants.BAR_HEIGHT, fillerBitmap);
    }

    updateMoney(money: number) {
        if (money < 0) {
            money *= -1;
            this.moneyText.setText(`$:-${this.padLeft(money.toString())}`);
        } else {
            this.moneyText.setText(`$:${this.padLeft(money.toString())}`);
        }
    }
    updateDone(done: number) {
        this.sequenceText.setText(`Seq:${done.toString()}`);
    }

    private padLeft(text) {
        const pad = 10;
        return new Array(pad - text.length + 1 > 0 ? pad - text.length + 1 : 0).join("0") + text;
    }

    putOnTop() {
        this.game.world.bringToTop(this.backgroundSprite);
        this.game.world.bringToTop(this.fillerSprite);
        this.game.world.bringToTop(this.moneyText);
        this.game.world.bringToTop(this.sequenceText);
    }

}