﻿"use strict";

import CompanyServiceProvider = require("Company/CompanyServiceProvider");
import FactServiceProvider = require("Facts/FactServiceProvider");
import Stage = require("Stages/Stage1");
import TrafficIntervall = require("Traffic/TrafficIntervall");
import TrafficOptions = require("Traffic/TrafficOptions");
import MagnitudeOrder = require("Stages/MagnitudeOrder");

export class EventServiceManager {
    private stage: Stage;
    private dialogAudioClip: Phaser.Sound = null;

    constructor(stage: Stage) {
        this.stage = stage;
    }

    registerEvents() {
        this.stage.pushToTopSignal.add(() => {
            this.stage.backgroundServiceProvider.putOnTop();
            this.stage.trafficServiceProvider.putOnTop();
            this.stage.meteoServiceProvider.putOnTop();
            this.stage.dialogManager.putOnTop();

            if (this.stage.topBar != null) {
                this.stage.topBar.putOnTop();
            }

        }, this);
        this.subscribeCompanyEvents();
        this.subscribeTimeEvents();
        this.subscribeGrowEvents();
        this.subscribeHighLightEvents();
        this.subscribeFactResolution();

    }

    private subscribeCompanyEvents() {
        this.stage.companyServiceProvider.moneyChangeSignal.add((money: number, done: number) => {
            this.stage.topBar.updateMoney(money);
            this.stage.topBar.updateDone(done);

        }, this);

        this.stage.companyServiceProvider.shitHappened.add((f: FactServiceProvider.Fact) => {
            this.stage.execute(f);
        }, this);

        this.stage.companyServiceProvider.gameOverSignal.addOnce(() => {
            this.stage.game.paused = true;
            console.log("game over");

        }, this);
    }

    private subscribeTimeEvents() {
        this.stage.dayNightServiceProvider.sunRiseSignal.add((day: Date) => {
            this.stage.factEnabled = true;
            this.stage.currentDate = day;
        }, this);
        this.stage.dayNightServiceProvider.moonRiseSignal.add((day: Date) => {
            //this.stage.factEnabled = false;

        }, this);

        this.stage.dayNightServiceProvider.clockSignal.add((hour: number) => {
            this.stage.companyServiceProvider.update(hour, this.stage.factEnabled);

            if (this.stage.factEnabled) {
                this.stage.companyServiceProvider.evaluateNewFact(hour);
            }
        }, this);

        this.stage.dayNightServiceProvider.minuteSignal.add((hour: number) => {
            var trafficOptions = new TrafficOptions.TrafficOptions(this.stage.currentDate, hour, true);
            trafficOptions.intervals.push(new TrafficIntervall.TrafficIntervall(() => { return (hour === 9) || (hour === 18); }, 150));
            this.stage.trafficServiceProvider.update(trafficOptions);
            this.stage.meteoServiceProvider.update(this.stage.currentDate ? this.stage.currentDate : new Date(), hour);
        }, this);
    }

    private subscribeGrowEvents() {
        this.stage.companyServiceProvider.growSignal0.add(() => {
            this.stage.backgroundServiceProvider.setMagnitude(new MagnitudeOrder({ x: 1, y: 1 }, { x: 0, y: 0 }));
        }, this);

        this.stage.companyServiceProvider.growSignal1.add(() => {
            this.stage.backgroundServiceProvider.setMagnitude(new MagnitudeOrder({ x: 1.1, y: 1.1 }, { y: -65, x: -15 }));
        }, this);

        this.stage.companyServiceProvider.growSignal2.add(() => {
            this.stage.backgroundServiceProvider.setMagnitude(new MagnitudeOrder({ x: 1.2, y: 1.2 }, { y: -132, x: -37 }));
        }, this);
    }

    private subscribeHighLightEvents() {
        this.stage.endSequenceHighLightSignal.add((fact: FactServiceProvider.Fact, idx: number) => {
            this.stage.dialogManager.highLightSequence(fact, idx);
        });

        this.stage.allSequenceHighLightSignal.add((fact: FactServiceProvider.Fact) => {
            this.stage.dontTrack = false;
            this.destroyDialog(fact);
        });
    }

    private subscribeFactResolution() {
        
    }

    private destroyDialog(fact: FactServiceProvider.Fact) {
        const elapse = this.reduceSequence(fact) * 300;
        const notification = new FactNotification (this.stage.game, fact, elapse);
        notification.onStopSignal.add(() => {
            const tween = this.stage.game.add.tween(this.stage.dialogButtons)
                .to({ y: this.stage.game.world.height + this.stage.dialogButtons.height }, 500, Phaser.Easing.Exponential.In, true);
            tween.onComplete.add(() => {
                if (this.dialogAudioClip == null) {
                    this.dialogAudioClip = this.stage.game.add.audio("zgump_9", 0.1, false);
                }

                this.stage.dialogButtons.destroy();
                this.stage.companyServiceProvider.factServiceProvider.evaluating = false;

            }, this);

        }, this);

        notification.start();
    }

    private reduceSequence(fact: FactServiceProvider.Fact): number {
        return fact.sequences.map((seq) => {
            return seq.cells.length;
        }, this).reduce((prev, next) => { return prev + next; });
    }

}

class FactNotification {
    timer: Phaser.Timer = null;
    messageText: Phaser.BitmapText = null;
    game: Phaser.Game = null;
    fact: FactServiceProvider.Fact = null;
    elapse: number = 0;
    private dialogAudioClip: Phaser.Sound = null;
    public onStopSignal: Phaser.Signal = null;

    constructor(game: Phaser.Game, fact: FactServiceProvider.Fact, elapse: number) {
        this.game = game;
        this.timer = game.time.create();
        this.elapse = elapse;
        this.messageText = this.game.add.bitmapText(0, 70, "digipip", "");
        this.messageText.fontSize = 100;
        this.onStopSignal = new Phaser.Signal();
    }

    start() {
        let mls = 0;
        this.timer.loop(Phaser.Timer.SECOND / 10,
            () => {
                mls++;
                const remain = (this.elapse - (mls * 100)) / 1000;
                const msg = remain + "";
                this.messageText.x = this.game.world.centerX - (this.messageText.width / 2);
                this.messageText.setText(msg);
            },
            this.timer);
        this.timer.start();
        this.game.time.events.add(this.elapse, () => {
            if (this.dialogAudioClip == null) {
                this.dialogAudioClip = this.game.add.audio("zgump_9", 0.1, false);
            }
            this.dialogAudioClip.play();
            if (this.messageText != null) {
                this.messageText.setText("");
            }
            this.onStopSignal.dispatch(this.fact);
            this.timer.stop();
            this.timer.destroy();
        });
    }
}

