﻿"use strict";

import Constants = require("Common/Constant");
import Helper = require("Helper");
import MagnitudeOrder = require("Stages/MagnitudeOrder");

class BackgroundServiceProvider {
    game: Phaser.Game;
    sunSprite: Phaser.Sprite;
    moonSprite: Phaser.Sprite;
    backgroundSprite: Phaser.Sprite;
    skylineImageGroup: Phaser.Group;
    palaceImageGroup: Phaser.Group;

    constructor(game: Phaser.Game) {
        this.game = game;
        this.backgroundSprite = this.game.add.sprite(0, 0, this.createBackground(this.game.width, this.game.height));
        this.initDayAndNightSprite();
        this.setSkyline();
        this.setPalace();
    }

    putOnTop() {
        this.game.world.bringToTop(this.skylineImageGroup);
        this.game.world.bringToTop(this.palaceImageGroup);
    }

    initDayAndNightSprite() {
        this.sunSprite = this.game.add.sprite(0, this.game.height, "sun");
        const specW = Constants.Constants.SPEC_WIDTH;
        const specH = Constants.Constants.SPEC_HEIGHT;
        const sunOrigW = this.sunSprite.width;
        const sunOrigH = this.sunSprite.height;
        const sunNewW = (this.game.width * sunOrigW) / specW;
        const sunNewH = (this.game.height * sunOrigH) / specH;

        this.moonSprite = this.game.add.sprite(this.game.width, - (this.game.height / 6) - sunNewW, "moon");

        const moonOrigW = this.moonSprite.width;
        const moonOrigH = this.moonSprite.height;
        const moonNewW = (this.game.width * moonOrigW) / specW;
        const moonNewH = (this.game.height * moonOrigH) / specH;

        this.sunSprite.width = sunNewW;
        this.sunSprite.height = sunNewH;
        this.moonSprite.width = sunNewW;
        this.moonSprite.height = sunNewH;
        this.sunSprite.scale.x = ((100 * sunNewW) / sunOrigW) / 100;
        this.sunSprite.scale.y = ((100 * sunNewH) / sunOrigH) / 100;
        this.moonSprite.scale.x = ((100 * moonNewW) / moonOrigW) / 150;
        this.moonSprite.scale.y = ((100 * moonNewH) / moonOrigH) / 150;
    }

    setSkyline() {
        this.skylineImageGroup = new Phaser.Group(this.game);
        let grey = 153;
        this.addSkylineLayer("skyline", 220, 150, Helper.RGBtoHEX(grey, grey, grey), true);

        grey = 102;
        this.addSkylineLayer("skyline", -220, 120, Helper.RGBtoHEX(grey, grey, grey), null);

        grey = 51;
        this.addSkylineLayer("skyline", -20, 50, Helper.RGBtoHEX(grey, grey, grey), true);

        grey = 25;
        this.addSkylineLayer("skyline", 0, 0, Helper.RGBtoHEX(grey, grey, grey), null);
    }

    setPalace() {
        this.palaceImageGroup = new Phaser.Group(this.game);
        const grey = 153;
        this.addPalaceLayer("palace", 0, 0, Helper.RGBtoHEX(grey, grey, grey), null);
        this.addPalaceLayer("door", 0, 0, Helper.RGBtoHEX(35, 35, 35), null);
        this.addPalaceLayer("glasses", 0, 0, Helper.RGBtoHEX(135, 234, 243), null);
        this.addPalaceLayer("windows", 0, 0, Helper.RGBtoHEX(135, 234, 243), null);
    }

    giveMeBackgroundSprites() {
        const backgroundSprites = [
            { sprite: this.backgroundSprite, from: 0x1f2a27, to: 0xB2DDC8 },
            { sprite: this.skylineImageGroup, from: 0x2f403b, to: 0x96CCBB }
        ];

        return backgroundSprites;
    }

    setMagnitude(magnitude: MagnitudeOrder) {
        this.game.add.tween(this.palaceImageGroup.scale)
            .to(magnitude.scale, 1000, Phaser.Easing.Bounce.InOut, true);

        this.game.add.tween(this.palaceImageGroup)
            .to(magnitude.position, 1000, Phaser.Easing.Bounce.InOut, true);

    }

    private createBackground(width: number, height: number) {
        let bgBitmap = this.game.add.bitmapData(this.game.width, this.game.height);
        bgBitmap.ctx.rect(0, 0, this.game.width, this.game.height);
        bgBitmap.ctx.fillStyle = "#33bbff";
        bgBitmap.ctx.fill();
        return bgBitmap;
    }

    private addPalaceLayer(imageName: string, x: number, y: number, tint?: number, flipX?: boolean) {
        const image = this.game.cache.getImage(imageName);
        const specW = Constants.Constants.SPEC_WIDTH;
        const specH = Constants.Constants.SPEC_HEIGHT;
        const origW = image.width;
        const origH = image.height;
        const newW = (this.game.width * origW) / specW;
        const newH = (this.game.height * origH) / specH;
        const s = this.game.add.tileSprite(flipX ? this.game.width + x : x, this.game.height - newH - y, origW, origH, imageName);
        s.scale.x = ((100 * newW) / origW) / 100;
        s.scale.y = ((100 * newH) / origH) / 100;

        if (flipX) {
            s.scale.x *= -1;
        }
        s.x = (this.game.width / 2) - (newW / 2);
        if (tint) {
            s.tint = tint;
        }
        this.palaceImageGroup.add(s);
    }

    private addSkylineLayer(imageName: string, x: number, y: number, tint?: number, flipX?: boolean) {
        const specW = Constants.Constants.SPEC_WIDTH;
        const specH = Constants.Constants.SPEC_HEIGHT;
        const origW = Constants.Constants.ORIG_WIDTH;
        const origH = Constants.Constants.ORIG_HEIGHT;
        const newW = (this.game.width * origW) / specW;
        const newH = (this.game.height * origH) / specH;
        const s = this.game.add.tileSprite(flipX ? this.game.width + x : x, this.game.height - newH - y, newW, newH, imageName);
        if (flipX) {
            s.scale.x *= -1;
        }
        if (tint) {
            s.tint = tint;
        }
        this.skylineImageGroup.add(s);
    }
}

export = BackgroundServiceProvider;