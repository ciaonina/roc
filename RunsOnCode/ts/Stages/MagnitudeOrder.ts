﻿"use strict";

class MagnitudeOrder {
    scale: { x: number, y: number };
    position: { x: number, y: number };
    constructor(scale: any, position: any) {
        this.scale = scale;
        this.position = position;
    }
}

export = MagnitudeOrder;