﻿"use strict";

import FactServiceProvider = require("Facts/FactServiceProvider");
import Helper = require("Helper");
import Constants = require("Common/Constant");
import Stage1 = require("Stages/Stage1");
import Sequencer = require("Facts/Sequencer");

export class DialogManager {
    stage: Stage1;
    messageText: Phaser.BitmapText = null;
    fact: FactServiceProvider.Fact = null;
    private dialogAudioClip: Phaser.Sound = null;
    private currentSequence: SequenceFindResult = null;

    constructor(stage: Stage1) {
        this.stage = stage;

    }

    drawButtons(fact: FactServiceProvider.Fact): void {
        this.fact = fact;
        if (this.dialogAudioClip == null) {
            this.dialogAudioClip = this.stage.game.add.audio("zgump_9", 0.1, false);
        }
        this.dialogAudioClip.play();

        const group = new Phaser.Group(this.stage.game);
        group.y = - this.stage.game.height;
        let fc = 0;
        for (let i = 0; i < 5; i++) {
            for (let j = 0; j < 5; j++) {
                group.add(this.drawButton(fact, fc, i, j));
                fc++;
                if (fc === fact.callToActions.length) {
                    break;
                }
            }
            if (fc === fact.callToActions.length) {
                break;
            }
        }
        this.stage.dialogButtons = group;
        const dialogTween = this.stage.game.add.tween(this.stage.dialogButtons)
            .to({ y: 0 }, 500, Phaser.Easing.Exponential.Out, true);
        dialogTween.onComplete.add(() => {
            this.highLightSequences(fact);
        }, this);
    }

    highLightSequences(fact: FactServiceProvider.Fact) {
        this.highLightSequence(fact, 0);
    }

    highLightSequence(f: FactServiceProvider.Fact, idx: number) {
        const fact = f;
        const latence = 200;
        if (idx < fact.sequences.length) {
            const s = fact.sequences[idx];
            s.cells.forEach((cell, cellIndex) => {
                const sprite = (this.stage.dialogButtons.children[cell.value - 1] as Phaser.Group)
                    .children[0] as Phaser.Sprite;
                const oldTint = 0xffd300;
                const tween = Helper.tweenTint(this.stage.game,
                    sprite,
                    sprite.tint,
                    Constants.Constants.BUTTON_HIGHLIGHT_COLOR,
                    latence * (cellIndex + 1),
                    Phaser.Easing.Default,
                    (latence) * ((cellIndex + 1) / 2));
                tween.onComplete.add(() => {
                    sprite.tint = oldTint;
                });
            });
            this.stage.game.time.events.add(s.cells.length * (latence),
                () => {
                    this.stage.endSequenceHighLightSignal.dispatch(fact, ++idx);
                },
                this);
        } else {
            this.stage.allSequenceHighLightSignal.dispatch(fact);
        }
    }

    putOnTop() {

        if (this.stage.dialogButtons != null) {
            this.stage.game.world.bringToTop(this.stage.dialogButtons);
        }

        if (this.messageText != null) {
            this.stage.game.world.bringToTop(this.messageText);
        }
    }

    private drawButton(fact: FactServiceProvider.Fact, fc: number, i: number, j: number): Phaser.Group {
        const gr = new Phaser.Group(this.stage.game);

        const btn = this.stage.game.add.sprite(0, 0, "button");
        btn.tint = Constants.Constants.BUTTON_BACKGROUND;
        btn.width = Constants.Constants.BUTTON_SIDE;
        btn.height = Constants.Constants.BUTTON_SIDE;
        btn.position.x = ((Constants.Constants.BUTTON_SIDE + Constants.Constants.BUTTON_MARGIN_SEPARATOR + 5) * j)
            + Constants.Constants.BUTTON_MARGIN_WIDTH;
        btn.position.y = ((Constants.Constants.BUTTON_SIDE + Constants.Constants.BUTTON_MARGIN_SEPARATOR + 5) * i)
            + Constants.Constants.BUTTON_MARGIN_HEIGHT;

        const btn1 = this.stage.game.add.sprite(0, 0, "button");
        btn1.tint = 0xffd300;
        btn1.width = Constants.Constants.BUTTON_SIDE + 8;
        btn1.height = Constants.Constants.BUTTON_SIDE + 8;
        btn1.position.x = btn.x - 4;
        btn1.position.y = btn.y - 4;

        const txt = this.stage.game.add.bitmapText(btn.position.x + (btn.width / 2), btn.position.y + (btn.height / 2), "digipip",
            fact.callToActions[fc].index.toString());
        txt.x -= (txt.width / 2);
        txt.y -= (txt.height / 2);
        txt.fontSize = 35;
        txt.font = Constants.Constants.FONT;
        txt.tint = 0xffffff;
        gr.add(btn1);
        gr.add(btn);
        gr.add(txt);

        btn.inputEnabled = true;
        btn.events.onInputDown.add(() => {
            txt.tint = 0x000000;
            const result = this.resolveInput(fact.callToActions[fc].index);
            const tween = Helper.tweenTint(this.stage.game, btn, btn.tint, result.tint, 300, Phaser.Easing.Default);
            tween.onComplete.add(() => {
                Helper.tweenTint(this.stage.game, btn, btn.tint, Constants.Constants.BUTTON_BACKGROUND, 300, Phaser.Easing.Default);
                txt.tint = 0xffffff;
            }, this);
            this.manageResult(result.value);

        }, this, null, txt);
        return gr;
    }

    private manageResult(result: number) {
        if (result >= 0) {
            this.stage.companyServiceProvider.company.$ -= result;
            console.log(this.stage.companyServiceProvider.company.done);
            //this.stage.companyServiceProvider.company.done ++;

            if (result > 0) {
                const notification = new ResultNotification(this.stage.game, 1000);
                notification.start(`-${result}`);
            }
        }
    }

    private findSequence(value: number): SequenceFindResult {
        for (let i = 0; i < this.fact.sequences.length; i++) {
            for (let j = 0; j < this.fact.sequences[i].cells.length; j++) {
                if (this.fact.sequences[i].cells[j].value === value) {
                    return new SequenceFindResult(value, this.fact.sequences[i]);
                }
            }
        }
        return null;
    }

    private computeSuccessResult(seq: SequenceFindResult): number {
        return seq.sequence.cells.length * 10;
    }

    private removeSequence(seq: SequenceFindResult) {
        this.fact.sequences.splice(this.fact.sequences.map(JSON.stringify).indexOf(JSON.stringify(seq.sequence)), 1);
    }

    private resolveInput(value): InputResult {
        const result = new InputResult();
        const seq = this.findSequence(value);
        result.value = 0;
        result.tint = eResultColor.red;
        if (seq == null) {
            this.currentSequence = null;
        } else {
            result.tint = eResultColor.yellow;
            if (this.currentSequence == null) {
                this.currentSequence = seq;
            }

            if (this.currentSequence.index + 1 === seq.index) {
                if (this.currentSequence.sequence
                    .cells[this.currentSequence.sequence.cells.length - 1].value === seq.index) {
                    console.log("complete");
                    result.value = this.computeSuccessResult(seq);
                    this.removeSequence(seq);
                    this.stage.companyServiceProvider.company.done++;
                    this.currentSequence = null;
                }
                this.currentSequence = seq;
            }
            else if (seq.sequence.cells[0].value === seq.index) {
                result.tint = eResultColor.green;
                this.currentSequence = seq;
            }
        }
        return result;
    }
}

enum eResultColor {
    red = 0xff0000,
    yellow = 0xffff66,
    green = 0x00ff00,
}

class InputResult {
    value: number = 0;
    tint: number = 0;
}

class SequenceFindResult {
    sequence: Sequencer.Sequence = null;
    index: number;
    constructor(index: number, sequence: Sequencer.Sequence) {
        this.index = index;
        this.sequence = sequence;
    }
}

class ResultNotification {
    timer: Phaser.Timer = null;
    messageText: Phaser.BitmapText = null;
    game: Phaser.Game = null;
    elapse: number = 0;
    private dialogAudioClip: Phaser.Sound = null;
    public onStopSignal: Phaser.Signal = null;

    constructor(game: Phaser.Game, elapse: number) {
        this.game = game;
        this.timer = game.time.create();
        this.elapse = elapse;
        this.messageText = this.game.add.bitmapText(0, 70, "digipip", "");
        this.messageText.fontSize = 50;
        this.onStopSignal = new Phaser.Signal();
    }

    start(message: string) {
        this.messageText.x = 10;
        this.messageText.setText(message);
        this.timer.start();
        this.game.time.events.add(this.elapse, () => {
            if (this.dialogAudioClip == null) {
                this.dialogAudioClip = this.game.add.audio("zgump_9", 0.1, false);
            }
            this.dialogAudioClip.play();
            if (this.messageText != null) {
                this.messageText.setText("");
            }
            this.onStopSignal.dispatch();
            this.timer.stop();
            this.timer.destroy();
        });
    }
}