﻿"use strict";

module DAL {

	// shim to support vendor prefix
	declare var window: {
		mozIndexedDB: IDBFactory;
		webkitIndexedDB: IDBFactory;
		indexedDB: IDBFactory;
		msIndexedDB: IDBFactory;
		IDBKeyRange: IDBKeyRange;
		webkitIDBKeyRange: IDBKeyRange;
		msIDBKeyRange: IDBKeyRange;
		IDBTransaction: IDBTransaction;
		webkitIDBTransaction: IDBTransaction;
		msIDBTransaction: IDBTransaction;
	}

	export interface ICFGRepository {
		Read(): CFG;
		Upsert(cfg: CFG): void;
		Delete(): void
	}

	export class DataLayer {
		private IdbFactory: IDBFactory = null;
		private IdbTransaction: IDBTransaction = null;
		private IdbKeyRange: IDBKeyRange = null;
		private request: IDBOpenDBRequest = null;
		private dbName = "NonOletDB";
		private version = 1;

		constructor() {
			if (window.indexedDB)
				this.IdbFactory = window.indexedDB;
			else if (window.mozIndexedDB)
				this.IdbFactory = window.mozIndexedDB;
			else if (window.webkitIndexedDB)
				this.IdbFactory = window.webkitIndexedDB;
			else if (window.msIndexedDB)
				this.IdbFactory = window.msIndexedDB;

			if (window.IDBTransaction)
				this.IdbTransaction = window.IDBTransaction;
			else if (window.webkitIDBTransaction)
				this.IdbTransaction = window.webkitIDBTransaction;
			else if (window.msIDBTransaction)
				this.IdbTransaction = window.msIDBTransaction;

			if (window.IDBKeyRange)
				this.IdbKeyRange = window.IDBKeyRange;
			else if (window.webkitIDBKeyRange)
				this.IdbKeyRange = window.webkitIDBKeyRange;
			else if (window.msIDBKeyRange)
				this.IdbKeyRange = window.msIDBKeyRange;

			if (this.IdbFactory == null || this.IdbTransaction == null || this.IdbKeyRange == null)
				throw new Error("we don't support your browser sorry");
		}

		private open() {
			let request: IDBOpenDBRequest = this.IdbFactory.open(this.dbName, this.version);

			request.onupgradeneeded = function (event) {
				//todo: create db structure
				this.result.createObjectStore("CFG", { keyPath: "cfg" });

			};

			request.onsuccess = function (event): IDBDatabase {
				return this.result;
			};

			request.onerror = function (event) {
				return this.result;
				//todo: log error
			};
		}

	}

	export class CFGRepository implements ICFGRepository {
		Read(): CFG {
			return null;
		}

		Upsert(cfg: CFG) {

		}

		Delete() {

		}
	}

	export class CFG {

	}
}

export = DAL;

