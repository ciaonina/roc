﻿"use strict";

class Helper {
    private game: Phaser.Game;

    constructor(game: Phaser.Game) {
        this.game = game;
    }

    static tweenTint(game: Phaser.Game, sprite: Phaser.Sprite, startColor: number, endColor: number, duration: number, easing: any, delay? : number): Phaser.Tween {
        const colorBlend = { step: 0 };
        const tween = game.add.tween(colorBlend).to({ step: 100 }, duration, easing ? easing : Phaser.Easing.Default, false, delay)
            .onUpdateCallback(() => {
                sprite.tint = Phaser.Color.interpolateColor(startColor, endColor, 100, colorBlend.step, 1);
            })
            .start();
        return tween;
    }
    
    static RGBtoHEX(r: number, g: number, b: number) {
        return r << 16 | g << 8 | b;
    }

    static capitalizeFirstLetter(string: string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    static RandomMinMax(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

}
export = Helper;