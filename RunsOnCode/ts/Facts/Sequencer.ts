﻿"use strict";

import P = require("Common/Promise");

module Sequencer {

    export interface Promise<Value> extends P.Promise<Value> { };

    export class Sequencer {
        private rowNumber: number;
        private columnNumber: number;
        matrix: Cell[][];
        private sequenceNumber: number;
        sequences: Sequence[];

        constructor(rows: number, columns: number, sequenceNumber: number) {
            this.rowNumber = rows;
            this.columnNumber = columns;
            this.matrix = [];
            this.sequenceNumber = sequenceNumber;
            this.sequences = [];
            this.build();
        }

        private build() {
            let value = 0;
            for (let i = 1; i <= this.rowNumber; i++) {
                this.matrix[i - 1] = [];
                for (let j = 1; j <= this.columnNumber; j++) {
                    ++value;
                    this.matrix[i - 1][j - 1] = new Cell(value);
                }
            }

            for (let i = 0; i < this.sequenceNumber; i++) {
                const seq = new Sequence();

                const rowSelected = this.randomMinMax(0, this.rowNumber - 1);
                seq.row = rowSelected;

                const seqLength = this.randomMinMax(3, this.columnNumber);

                const startAt = this.randomMinMax(0, this.columnNumber - seqLength);

                const endAt = startAt + seqLength;

                for (let j = startAt; j <= endAt - 1; j++) {
                    seq.cells.push(this.matrix[rowSelected][j]);
                }
                this.sequences.push(seq);
            }
        }

        printGrid(): HTMLTableElement {
            const t = new HTMLTableElement();
            for (let i = 0; i < this.rowNumber; i++) {
                const currentRow = t.insertRow();
                for (let j = 0; j < this.columnNumber; j++) {
                    const currentCell = currentRow.insertCell();
                    currentCell.innerText = this.matrix[i][j].value.toString();
                }
            }

            return t;
        }

        randomMinMax(min: number, max: number): number {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    }

    export class Sequence {
        cells: Cell[];
        row: number;

        constructor() {
            this.cells = [];
        }
    }

    export class Cell {
        value: number;
        constructor(value: number) {
            this.value = value;
        }
    }


}

export = Sequencer;

//window.onload = () => {
//    var el = document.getElementById("content");
//    const g = new SequencesGrid(5, 5, 5);
//    g.build();
//};