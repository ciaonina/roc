﻿import Helper = require("Helper");
import Sequencer = require("Facts/Sequencer");
module Facts {
    "use strict";
    export class FactServiceProvider {
        evaluating = false;

        createNewFact() {
            return FactBuilder.create();
        }
    }
    export class FactBuilder {
        static create(): Fact {
            // let result: Fact;
            // switch (Helper.RandomMinMax(eTypeFact.Task, eTypeFact.Release - 1)) {
            //    case eTypeFact.Task:
            //        result = new Task();
            //        break;
            //    case eTypeFact.CriticalBugFix:
            //        result = new CriticalBugFix();
            //        break;
            //    case eTypeFact.Release:
            //        result = new Release();
            //        break;
            // }
            // result = 

            return new Task();
        }
    }
    export enum eTypeFact {
        Task,
        CriticalBugFix,
        Release
    }
    export abstract class Fact {
        callToActions: CallToAction[] = [];
        sequences: Sequencer.Sequence[];

        constructor() {
            this.callToActions = [];
            this.sequences = [];
        }

        private static products: string[] = [
            "X09-iEvil Mass-mailer",
            "Rev-Look-Scam-Atrocius",
            "Octo-Retro-Injector-u6i8",
            "SATAN-BPM",
            "Micro-Scan-Suppressor-CALIBER"
        ];
        abstract getStatement(): string;
        protected abstract addCallToActions(ctd: CallToAction): void;
        protected getProductName(): string {
            return Fact.products[Helper.RandomMinMax(0, Fact.products.length - 1)];
        }
        protected shuffleCallToActions() {
            for (let index = this.callToActions.length - 1; index >= 0; index--) {
                const randomIndex = Math.floor(Math.random() * (index + 1));
                const itemAtIndex = this.callToActions[randomIndex];
                this.callToActions[randomIndex] = this.callToActions[index];
                this.callToActions[index] = itemAtIndex;
            }
        }
    }
    export class Task extends Fact {
        constructor() {
            super();
            const sequencer = new Sequencer.Sequencer(5, 5, 5);
            for (let rowIndex = 0; rowIndex < sequencer.matrix.length; rowIndex++) {
                const row = rowIndex;
                sequencer.sequences.filter((item) => { return item.row === row }).map((si) => {
                    this.sequences.push(si);
                }, this);
                for (let column = 0; column < sequencer.matrix[row].length; column++) {
                    const val = sequencer.matrix[row][column].value;
                    this.addCallToActions(new SimpleCallToAction(val, val.toString()));
                }
            }
        }
        getStatement(): string {
            return `You have to develop a new feature of ${this.getProductName()}`;
        }
        addCallToActions(cta: CallToAction): void {
            this.callToActions.push(cta);
        }
    }
    export class CriticalBugFix extends Fact {
        constructor() {
            super();
        }
        getStatement(): string {
            return `You  have to fix a critical bug of ${this.getProductName()}`;
        }
        addCallToActions(): void {
            this.callToActions.push(null);
        }
    }
    export class Release extends Fact {
        constructor() {
            super();
        }
        getStatement(): string {
            return `Its time to deploy a new release of ${this.getProductName()}`;
        }
        addCallToActions(): void {
            this.callToActions.push(null);
        }
    }

    export abstract class CallToAction {
        index: number;
        text: string;
        constructor(index: number, text: string) {
            this.index = index;
            this.text = text;
        }
    }
    export class SimpleCallToAction extends CallToAction {

    }
    export class SimpleBug extends CallToAction {

    }
}

export = Facts;