﻿"use strict";

class Boot extends Phaser.State {

    preload() {
        this.game.load.image("loading", "Assetts/Images/ui/loading_bar.png");
    }
    create() {
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
        this.scale.forceOrientation(false,true);
        this.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.state.start("Preloader", true, false);
    }
}
export = Boot;