﻿import Helper = require("Helper");

module Constants {
    "use strict";
    export class Constants {
        static DAYLIGHT = 10000;

        static $STATE_1 = 1000;
        static $STATE_2 = 2000;
        static $GAMEOVER = 3000;

        static MONEY_START = 0;
        static MONEY_MIN = 1;
        static MONEY_MAX = 10;

        static FONTSIZE = 30;
        static FONT = "digipip";

        static SPEC_WIDTH = 768;
        static SPEC_HEIGHT = 1024;
        static ORIG_WIDTH = 800;
        static ORIG_HEIGHT = 600;

        static BUTTON_SIDE = 60;
        static BUTTON_MARGIN_WIDTH = 20;
        static BUTTON_MARGIN_HEIGHT = 200;
        static BUTTON_MARGIN_SEPARATOR = 5;
        static BUTTON_BACKGROUND = 0x333333;
        static BUTTON_BACKGROUND_CLICK = Helper.RGBtoHEX(235, 235, 60);
        static BUTTON_HIGHLIGHT_COLOR = 0xff0066;//0x0099ff;

        static BAR_HEIGHT = 50;
        static BAR_BACKGROUND_COLOR = "#333333";
        static BAR_FILLER_HEIGHT = 5;
        static BAR_FILLER_BACKGROUND_COLOR = "#ffd300";

        static MUSIC_ENABLED = false;
    }
}

export = Constants;