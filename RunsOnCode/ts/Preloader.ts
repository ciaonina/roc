﻿"use strict";

import Helper = require("Helper");
class Preloader extends Phaser.State {
    preloadBar: Phaser.Sprite;

    preload() {
        const sprite = this.add.sprite(0, 0, "loading");
        this.stage.setBackgroundColor(Helper.RGBtoHEX(0, 102, 153));
        sprite.scale.setTo(.5, .5);
        sprite.x = this.game.width / 2 - (sprite.width / 4);
        sprite.y = this.game.height / 2 - (sprite.height / 4);
        this.preloadBar = sprite;

        this.load.setPreloadSprite(this.preloadBar);
        
        this.load.bitmapFont("digipip", "Assetts/Fonts/font.png", "Assetts/Fonts/font.xml");

        this.load.audio("bangalore", "Assetts/Sounds/bangalore.mp3");
        this.load.audio("thunder", "Assetts/Sounds/thunder.wav");
        this.load.audio("rushhour", "Assetts/Sounds/rushhour1.wav");
        this.load.audio("rainroof", "Assetts/Sounds/rainroof.wav");

        this.load.audio("music_1", "Assetts/Musics/55406__kathakaku__provided-1-pattern.wav");

        this.load.audio("zgump_3", "Assetts/Sounds/synth/264553__zgump__synths-01-03.wav");
        this.load.audio("zgump_5", "Assetts/Sounds/synth/264559__zgump__synths-01-05.wav");
        this.load.audio("zgump_9", "Assetts/Sounds/synth/264561__zgump__synths-01-09.wav");


        this.load.image("skyline", "Assetts/Images/environment/Skyline.png");
        this.load.image("moon", "Assetts/Images/environment/moon.png");
        this.load.image("sun", "Assetts/Images/environment/sun.png");
        this.load.image("cloud1", "Assetts/Images/clouds/cloud1.png");
        this.load.image("cloud2", "Assetts/Images/clouds/cloud2.png");
        this.load.image("cloud3", "Assetts/Images/clouds/cloud3.png");
        this.load.image("cloud4", "Assetts/Images/clouds/cloud4.png");
        this.load.image("cloud5", "Assetts/Images/clouds/cloud5.png");
        this.load.image("cloud6", "Assetts/Images/clouds/cloud6.png");

        this.load.image("palace", "Assetts/Images/palace/palace.png");
        this.load.image("door", "Assetts/Images/palace/door.png");
        this.load.image("glasses", "Assetts/Images/palace/glass.png");
        this.load.image("windows", "Assetts/Images/palace/windows.png");

        this.load.image("car-1", "Assetts/Images/traffic/car-1.png");
        this.load.image("car-2", "Assetts/Images/traffic/car-2.png");
        this.load.image("car-3", "Assetts/Images/traffic/car-3.png");
        this.load.image("car-4", "Assetts/Images/traffic/car-4.png");
        this.load.image("autobus", "Assetts/Images/traffic/autobus500.png");

        this.load.image("button", "Assetts/Images/ui/button.png");

    }

    create() {
        const tween = this.add.tween(this.preloadBar).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
        tween.onComplete.add(this.startStage1, this);
    }

    startStage1() {
        this.game.state.start("Stage1", true, false);
    }
}

export = Preloader;